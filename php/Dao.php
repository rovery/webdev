<?php
// Dao.php
// class for saving and getting comments from MySQL
class Dao {

  private $host = "us-cdbr-east-04.cleardb.com";
  private $db = "heroku_5e96e31dcaba257 ";
  private $user = "b2bfe0612d633e";
  private $pass = "3249ed42";

  public function getConnection () {
    return
      new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user,
          $this->pass);
  }

  public function saveComment ($comment) {
    $conn = $this->getConnection();
    $saveQuery =
        "INSERT INTO comment
        (comment)
        VALUES
        (:comment)";
    $q = $conn->prepare($saveQuery);
    $q->bindParam(":comment", $comment);
    $q->execute();
  }

  public function getUser($email){
    $conn = $this->getConnection();
    return $conn->query( 
      "SELECT * FROM USER WHERE
      email = 'robbyovery@gmail.com'" )   
  }

  public function getComments () {
    $conn = $this->getConnection();
    return $conn->query("SELECT * FROM comment");
  }
} // end Dao